'use strict';

// Set the 'production' environment configuration object
module.exports = {
  dbURI: 'http://admin:1q2w3e4r@18.196.26.155:5984/books/',
  sessionSecret: 'productionSessionSecret',
  proxyHost: 'http://localhost',
  proxyPort: 3001
};