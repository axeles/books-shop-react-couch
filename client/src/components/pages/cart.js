"use strict";

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Panel,
  Row,
  Col,
  Button,
  ButtonGroup,
  Modal,
  Label
} from 'react-bootstrap';

import {getCart, deleteCartItem, updateCartItem} from "../../actions/cartActions";

class Cart extends Component {

  constructor(...args){
    super(...args);
    this.state = { showModal: false };
  }

  componentDidMount() {
    this.props.getCart();
  }

  close() {
    this.setState({ showModal: false });
  };

  open() {
    this.setState({ showModal: true });
  };

  onDelete(_id){
    this.props.deleteCartItem(_id, this.props.cart);
  }

  onIncreaseQuantity(_id, qty){
    this.props.updateCartItem(_id, qty + 1, this.props.cart);
  }

  onDecreaseQuantity(_id, qty){
    this.props.updateCartItem(_id, qty > 1 ? qty -1 : qty, this.props.cart);
  }

  renderCart(){
    const cartItemsList = this.props.cart.map(function (item) {
      return (
        <Panel key={item._id}>
          <Row>
            <Col xs={12} sm={3}>
              <h4>{item.title}</h4><span>    </span>
            </Col>
            <Col xs={12} sm={2}>
              <h4>$ {item.price}</h4>
            </Col>
            <Col xs={12} sm={2}>
              <Button className="left-button" onClick={ this.onDecreaseQuantity.bind(this, item._id, item.quantity)} bsStyle="default" bsSize="small">-</Button>
              <h4><span className="cart-label">{ item.quantity }</span></h4>
              <Button className="right-button"  onClick={ this.onIncreaseQuantity.bind(this, item._id, item.quantity)} bsStyle="default" bsSize="small">+</Button>
            </Col>
            <Col xs={12} sm={3}>
              <h4>Sum $ &nbsp;<Label bsStyle="success">{ (item.quantity * item.price).toFixed(2) }</Label></h4>
            </Col>
            <Col xs={12} sm={2}>
              <ButtonGroup style={{minWidth: '200px'}}>
                <Button className="cart-button" onClick={ this.onDelete.bind(this, item._id)} bsStyle="danger" bsSize="small">DELETE</Button>
              </ButtonGroup>
            </Col>
          </Row>
        </Panel>
      )
    }, this);
    return (
      <Panel header="Cart" bsStyle="primary">
        {cartItemsList}
        <Row>
          <Col xs={12}>
            <h4>Total amount: ${this.props.totalAmount}</h4>
            <Button onClick={this.open.bind(this)} bsStyle="success" bsSize="small">
              PROCEED TO CHECKOUT
            </Button>
          </Col>
        </Row>
        <Modal show={this.state.showModal} onHide={this.close.bind(this)}>
          <Modal.Header closeButton>
            <Modal.Title>Thank You!</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h4>Your order has been saved.</h4>
            <p>You will receive email confirmation.</p>
          </Modal.Body>
          <Modal.Footer>
            <Col>
              <h4>Total amount: ${this.props.totalAmount}</h4>
            </Col>
            <Button onClick={this.close.bind(this)}>Close</Button>
          </Modal.Footer>
        </Modal>
      </Panel>
    )

  };

  emptyCart(){
    return (
      <div className="empty">Your cart is empty</div>
    )
  };

  render(){
    if(this.props.cart[0]){
      return this.renderCart();
    } else {
      return this.emptyCart();
    }
  }
}

function mapStateToProps(state){
  return {
    cart: state.cartReducers.cart,
    totalAmount: state.cartReducers.totalAmount
  }
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getCart,
    deleteCartItem,
    updateCartItem
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
