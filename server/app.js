'use strict';

// Set the 'NODE_ENV' variable
process.env.NODE_ENV = process.env.NODE_ENV || 'development';
if(process.env.NODE_ENV === 'development') {
  process.env.DEBUG = 'books-shop-react-couch:server';
}

// Load the module dependencies
const express  = require('./config/express');

// Create a new Express application instance
const app = express();

// Use the module.exports property to expose our Express application instance for external usage
module.exports = app;