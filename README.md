# Annotation
This app implements a simple books shop. It uses NodeJS and Express for building back-end server and API server.
It uses CouchDB as a storage for books' data and users sessions.
As the front-end part of the app we used ReactJS and Redux.
  
# Quick start

### clone project from repo
```bash
git clone https://axeles@bitbucket.org/axeles/books-shop-react-couch.git

```
### change directory to the project's
```bash
cd books-shop-react-couch
```

### install the app dependencies with npm
```bash
npm install
```

### create DB for books and ops_sessions
```bash
curl -X PUT http://localhost:5984/books
```

### start the application 
```bash
npm start
```
After the app's started go to [http://0.0.0.0:3000](http://0.0.0.0:3000) or [http://localhost:3000](http://localhost:3000) in your browser

## File Structure
```
 ├──client/                        * front-end app folder
 │   ├──src/                       * front-end source files that will be compiled to javascript
 │      ├──actions/                * a simple test of components in app.component.ts
 │      ├──components/             * a simple end-to-end test for /
 │      └──reducers/               * a simple version of our App component components
 │      └──app.js                  * a simple version of our App component components
 │      └──main.js                 * a simple version of our App component components
 │
 ├──public/                        * public assets are served here
 |   ├──css/                       * stylesheets
 |   ├──img/                       * images
 |   ├──js/                        * js scripts
 |   ├──favicon.ico                * favicon 
 │
 ├──server/                        * server
 |   ├──api/                       * api (DAO) layer
 |   ├──bin/                       * runner of the server
 |   ├──config/                    * config files
 |   ├──routes/                    * server routes
 |   ├──utils/                     * reusable utils
 |   ├──views/                     * views of the server
 |   ├──app.js                     * main server file 
 |   ├──apiServer.js               * API server file 
 │
 ├──.gitignore                     * gitignore config
 ├──package.json                   * what npm uses to manage it's dependencies
 ├──README.md                      * project description file
 └──webpack.config.js              * webpack main configuration file

```

# Getting Started 
## Dependencies
What you need to run this app:
* `node`
* `npm`
* `CouchDB`

## Install CouchDB 
You can get CouchDB here: http://couchdb.apache.org/

Documentation for CouchDB: http://docs.couchdb.org/

## Run CouchDB
````bash
sudo -i -u couchdb /home/couchdb/bin/couchdb

````

## Create DBs ops_db and ops_sessions using curl
````bash
curl -X PUT http://localhost:5984/ops_db
curl -X PUT http://localhost:5984/ops_sessions

```` 
## or using Fauxton
http://127.0.0.1:5984/_utils/index.html

# Running the app
After you have installed all dependencies you can now run the app. 
Open a terminal window and run 
````bash
`npm start`
````
to start the server and API server.
The port will be displayed to you in the terminal you run `npm start`.

# For development
## Install global packages
You should install required global dependencies:
```bash
npm install --global webpack
```

### start the server 
```bash
npm run dev
```

### start the api server 
```bash
npm run proxy
```

### start webpack 
```bash
webpack
```

