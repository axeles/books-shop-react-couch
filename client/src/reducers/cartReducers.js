export function cartReducers(state = {cart: []}, action){

  switch (action.type) {
    case 'ADD_TO_CART':
      return {
        ...state,
        cart: action.payload,
        totalAmount: totals(action.payload).amount,
        totalQuantity: totalQuantity(action.payload).totalQuantity
      };
    case 'DELETE_FROM_CART':
      return {
        ...state,
        cart: action.payload,
        totalAmount: totals(action.payload).amount,
        totalQuantity: totalQuantity(action.payload).totalQuantity
      };
    case 'UPDATE_CART_ITEM':
      return {
        ...state,
        cart: action.payload,
        totalAmount: totals(action.payload).amount,
        totalQuantity: totalQuantity(action.payload).totalQuantity
      };
  }
  return state;
}

// CALCULATE TOTALS
export function totals(cartItems){
  const totalAmout = cartItems.map(function (item) {
    return item.price * item.quantity;
  }).reduce(function (a, b) {
    return a + b;
  }, 0);
  return { amount: totalAmout.toFixed(2) };
}

// CALCULATE TOTAL QUANTITY
export function totalQuantity(cartItems){
  const totalQuantity = cartItems.map(function (item) {
    return item.quantity;
  }).reduce(function (a, b) {
    return a + b;
  }, 0);
  return { totalQuantity };
}