// Invoke 'strict' JavaScript mode
'use strict';

// Set the 'development' environment configuration object
module.exports = {
	dbURI: 'http://admin:1q2w3e4r@18.196.26.155:5984/books/',
	dbHost: '18.196.26.155',
	dbPort: '5984',
    sessionDbName: 'sessions',
	username: 'admin',
	password: '1q2w3e4r',
	sessionSecret: 'developmentSessionSecret',
	proxyHost: 'http://localhost',
	proxyPort: 3001,
};