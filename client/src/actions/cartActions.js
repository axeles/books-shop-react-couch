"use strict";

import axios from 'axios';

// ADD TO CART
export function getCart(){
  return function (dispatch) {
    axios.get('/api/cart')
      .then(function (response) {
        dispatch({type: 'ADD_TO_CART', payload: response.data})
      })
      .catch(function (err) {
        dispatch({type: 'GET_CART_REJECTED', payload: 'Cannot get the cart: ' + err})
      })
  }
}

// ADD TO CART
export function addToCart(cart){
  return function (dispatch) {
    axios.post('/api/cart', cart)
      .then(function (response) {
        dispatch({type: 'ADD_TO_CART', payload: response.data})
      })
      .catch(function (err) {
        dispatch({type: 'ADD_TO_CART_REJECTED', payload: 'Cannot save the cart: ' + err})
      })
  }
}

// DELETE FROM CART
export function deleteCartItem(_id, cart){
  // Determine at which index in the cart array is the item to be deleted
  const indexOfItemToDelete = cart.findIndex(function (item) {
    return item._id === _id;
  });
  const updatedCart = [...cart.slice(0, indexOfItemToDelete),
    ...cart.slice(indexOfItemToDelete + 1)];

  return function (dispatch) {
    axios.post('/api/cart', updatedCart)
      .then(function (response) {
        dispatch({type: 'DELETE_FROM_CART', payload: response.data})
      })
      .catch(function (err) {
        dispatch({type: 'DELETE_FROM_CART_REJECTED', payload: 'Cannot delete item from the cart: ' + err})
      })
  }
}
// UPDATE CART ITEM
export function updateCartItem(_id, qty, cart){
  // Determine at which index in the cart array is the item to be updated
  const indexOfItemToUpdate = cart.findIndex(function (item) {
    return item._id === _id;
  });
  const updatedItem =
    {
      _id: cart[indexOfItemToUpdate]._id,
      title: cart[indexOfItemToUpdate].title,
      description: cart[indexOfItemToUpdate].description,
      price: cart[indexOfItemToUpdate].price,
      quantity: qty
    };
  const updatedCart = [...cart.slice(0, indexOfItemToUpdate), updatedItem,
    ...cart.slice(indexOfItemToUpdate + 1)];

  return function (dispatch) {
    axios.post('/api/cart', updatedCart)
      .then(function (response) {
        dispatch({type: 'UPDATE_CART_ITEM', payload: response.data})
      })
      .catch(function (err) {
        dispatch({type: 'UPDATE_CART_ITEM_REJECTED', payload: 'Cannot save the cart: ' + err})
      })
  }
}
