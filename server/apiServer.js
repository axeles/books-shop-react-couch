'use strict';

const express      = require('express');
const path         = require('path');
const favicon      = require('serve-favicon');
const logger       = require('morgan');
const compress     = require('compression');
const cookieParser = require('cookie-parser');
const bodyParser   = require('body-parser');
const session      = require('express-session');
const config       = require('./config/config');
const ConnectCouchDB = require('connect-couchdb')(session);
const sassMiddleware = require('node-sass-middleware');

// Create a new Express application instance
const app = express();

// TODO: repair saving session's data (it not works yet)
const store = new ConnectCouchDB({
    // Name of the database you would like to use for sessions.
    name: config.sessionDbName,
    host: config.dbHost,
    port: config.dbPort,
    username: config.username,
    password: config.password,

    // Optional. How often expired sessions should be cleaned up.
    // Defaults to 600000 (10 minutes).
    // reapInterval: 600000,

    // Optional. How often to run DB compaction against the session
    // database. Defaults to 300000 (5 minutes).
    // To disable compaction, set compactInterval to -1
    // compactInterval: 300000,

    // Optional. How many time between two identical session store
    // Defaults to 60000 (1 minute)
    // setThrottle: 60000
});

// console.log('+++ Store', store);

// Use the 'NODE_ENV' variable to activate the 'morgan' logger or 'compress' middleware
if (process.env.NODE_ENV === 'development') {
  app.use(logger('dev'));
} else if (process.env.NODE_ENV === 'production') {
  app.use(compress());
}

app.use(favicon(path.join(__dirname, '../public', 'favicon.ico')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// Configure the 'session' middleware
app.use(session({
  saveUninitialized: false,
  resave: false,
  secret: config.sessionSecret,
  cookie: {maxAge: 1000 * 60 * 60 * 24 * 2}, // 2 days in milliseconds
  store: store
}));

app.use(cookieParser());
app.use(sassMiddleware({
  src: path.join(__dirname, './views/styles'),
  dest: path.join(__dirname, '../public/css'),
  prefix: '/css',
  debug: true,
  outputStyle: 'compressed',
  indentedSyntax: true,
  sourceMap: true
}));

// Configure static file serving
app.use(express.static(path.join(__dirname, '../public')));
app.use(express.static(path.join(__dirname, '../node_modules')));

// Load the routing files
require('./api/routes/books.api.routes')(app);
require('./api/routes/cart.api.routes')(app);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function (err, req, res) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render(err);
});

app.listen(config.proxyPort, function (err) {
  if(err) {
    return console.log(err);
  }
  console.log(`API server is listening on ${config.proxyHost}:${config.proxyPort}`);
});
