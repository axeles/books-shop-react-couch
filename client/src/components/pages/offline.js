"use strict";

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import axios from 'axios';

import {
  MenuItem,
  InputGroup,
  DropdownButton,
  Image,
  Col,
  Row,
  Well,
  Panel,
  FormControl,
  FormGroup,
  ControlLabel,
  Button
} from 'react-bootstrap';
import { findDOMNode } from 'react-dom';
import { postBook, deleteBook, getBooks, resetButton } from "../../actions/bookActions";

class Offline extends Component {

  render(){
    return (
      <Well>
        <Row>
          <Col xs={12} sm={6}>
            <Panel>
              <h2>We're sorry, this page hasn't been cached yet :/</h2>
              <p>But why don't you try one of our <a href="/">other pages</a>?</p>
            </Panel>
          </Col>
        </Row>
      </Well>
    );
  }
}

export default Offline;
