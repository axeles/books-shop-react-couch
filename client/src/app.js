'use strict';

import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

// REDUX MIDDLEWARE
import thunk from 'redux-thunk';
import logger  from 'redux-logger';
// IMPORT COMBINED REDUCERS
import reducers from './reducers/index';
// APP COMPONENTS
import Main from './main';
import BooksList from './components/pages/booksList';
import BookForm from './components/pages/bookForm';
import Cart from './components/pages/cart';
import Offline from "./components/pages/offline";

// CREATING STORE
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const middleware = applyMiddleware(thunk, logger);
const store = createStore(reducers, /* preloadedState, */ composeEnhancers(middleware));


const Routes = (
  <Provider store={store}>
    <Router history={browserHistory}>
      <Route path="/" component={Main}>
        <IndexRoute component={BooksList} />
        <Route path="/admin" component={BookForm} />
        <Route path="/cart" component={Cart} />
        <Route path="/offline" component={Offline} />
      </Route>
    </Router>
  </Provider>
);
render(Routes, document.getElementById('app')
);

