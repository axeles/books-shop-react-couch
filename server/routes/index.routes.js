const packageJson = require('../../package.json');

module.exports = function(app) {
  app.get('*', function (req, res) {
    res.render('index', {title: packageJson.description});
  });
};


