'use strict';

const fs = require('fs');
const path = require('path');
const axios = require('axios');
const uuidv1 = require('uuid/v1');
const utils = require('../../utils/index');
const config = require('../../config/config');
// const nano = require('nano')(config.dbHost);
// const db = nano.db.use(config.dbName);

// ===== Create a new controller method that list of all books ==========
exports.booksList = function (req, res) {
  const params = '?include_docs=true';
  axios.get( config.dbURI + '_all_docs' + params)
    .then(function (response) {
      // console.log('+++ response.data.rows: ', response.data.rows);
      const docs = [];
      response.data.rows.forEach(function(item){
        docs.push(item.doc);
      });
      utils.sendJsonResponse(res, 200, docs);
    })
    .catch(function(err){
      console.log('+++ Error while fetching book list: ', err);
      utils.sendJsonResponse(res, 404, err);
    })
};

// ===== Create a new controller method that that creates new book ==========
module.exports.booksCreateOne = function (req, res) {
  const uuid = uuidv1();
  const book = req.body;

  axios({
      method: 'put',
      url: config.dbURI + uuid,
      data: book
    })
    .then(function (response) {
      utils.sendJsonResponse(res, 201, response.data);
    })
    .catch(function (err) {
      console.log('+++ Error while creating the book',err);
      utils.sendJsonResponse(res, 400, err);
    });
};

// ===== Create a new controller method that returns the book with given bookID ======
exports.booksReadOne = function (req, res) {

  if (!req.params || !req.params.bookId) {
    utils.sendJsonResponse(res, 404, {"message": "No bookId in request"});
  }

  axios.get( config.dbURI + req.params.bookId)
    .then(function (response) {
      console.log('+++ get response: ', response.data);
      utils.sendJsonResponse(res, 200, response.data);
    })
    .catch(function(err){
      console.log('+++ Error while getting the book: ', err);
      utils.sendJsonResponse(res, 404, err);
    })
};

// ===== Create a new controller method that updates the book with given bookID ======
exports.booksUpdateOne = function (req, res) {
  if (!req.params.bookId) {
    utils.sendJsonResponse(res, 404, {
      "message": "Not found, bookId is required"
    });
  }
};

// ===== Create a new controller method that deletes the book with given bookID ======
exports.booksDeleteOne = function (req, res) {
  if (!req.params || !req.params.bookId || !req.body) {
    utils.sendJsonResponse(res, 404, {"message": "No bookId or _rev in request"});
  }

  console.log('+++ params: ', req.params);
  console.log('+++ body: ', req.body);

  const bookId = req.params.bookId;
  const params = '?rev=' + req.body._rev;
  const query = config.dbURI + bookId + params;

  axios.delete(query)
    .then(function (response) {
      utils.sendJsonResponse(res, 201, response.data);
    })
    .catch(function (err) {
      console.log('+++ Error while deleting the book',err);
      utils.sendJsonResponse(res, 400, err);
    });


  if (bookId) {
  } else {
    utils.sendJsonResponse(res, 404, {"message": "No bookId"});
  }
};

// ===== Create a new controller method that fetch images from the file system ==========
module.exports.getImagesList = function (req, res) {
  const imgFolder = path.join(__dirname,  '../../../public/img');
  console.error.bind(console, '+++ imgFolder', imgFolder);
  fs.readdir(imgFolder, function (err, files) {
    if(err){
      console.error(err);
    }
    const images = [];
    files.forEach(function (file) {
      images.push({ name: file });
    });
    utils.sendJsonResponse(res, 201, images);
  });
};

